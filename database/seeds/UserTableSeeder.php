<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;

class UserTableSeeder extends Seeder
{
    private $types = [
        'admin',
        'user',
        'author',
        'owner'
    ];

    public function run()
    {
        $faker = Faker::create();
        foreach(range(0,20) as $i){
            $fullname = $faker->name;
            DB::table('users')->insert([
                'name' => $fullname,
                'email' => $faker->email,
                'password' => Hash::make('12345678'),
                'type' => $this->types[array_rand($this->types)],
                'bio' => 'This is '.$fullname,
                'photo' => 'user.png'
            ]);
        }
    }
}
