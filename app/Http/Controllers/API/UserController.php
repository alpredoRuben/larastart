<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;

class UserController extends Controller
{
    /** CONSTRUCTOR */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    

    /** GET ALL USERS WITH LAST INSERT */
    public function index()
    {
        //$this->authorize('isAdmin');
        if(\Gate::allows('isAdmin') || \Gate::allows('isAuthor')) {
            
            //return User::latest()->paginate(5);
            $data = User::paginate(5);
    	    return response()->json($data);
        }

    }

    /** INSERT USER */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users',
            'password' => 'required|string|min:8',
            'type' => 'required|string',
            'bio' => 'required'
        ]);

        return User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'type' => $request['type'],
            'bio' => $request['bio'],
            'photo' => $request['photo'],
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);
    }

    /** GET USER BY ID TO SHOW PROFILE */
    public function profile()
    {
        return auth('api')->user();
    }

    /** UPDATE PROFILE */
    public function updateProfile(Request $request)
    {
        $user = auth('api')->user();
        $current_photo = $user->photo;

        $this->validate($request, [
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users,email,'.$user->id,
            'password' => 'sometimes|required|min:8',
            'type' => 'required|string',
            'bio' => 'required'
        ]);
        
        if($request->photo != $current_photo)
        {
            $photo_name = time(). '.'. explode('/', explode(':',substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];
            \Image::make($request->photo)->save(public_path('img/profile/').$photo_name);
            $request->merge(['photo' => $photo_name]);

            $photo_file = public_path('img/profile').$current_photo;

            if(file_exists($photo_file)) {
                @unlink($photo_file);
            }

        }

        if(!empty($request->password)) {
            $request->merge(['password' => Hash::make($request->password)]);
        }

        try {
            $user->update($request->all());
            return [ 'message' => 'Update data profile success'];
        } catch (ModelNotFoundException $th) {
            return [ 'message' => $e->getMessage() ];
        }       
    }

    public function show($id)
    {
        
    }

    /** UPDATE DATA USER BY ID AND REQUEST DATA*/
    public function update(Request $request, $id)
    {
        try {
            $user = User::findOrFail($id);

            $this->validate($request, [
                'name' => 'required|string|max:191',
                'email' => 'required|string|email|max:191|unique:users,email,'.$user->id,
                'password' => 'sometimes|min:8',
                'type' => 'required|string',
                'bio' => 'required'
            ]);
            
            $user->update($request->all());
            return [ 'message' => 'Update user successfully', 'option' => $request->all() ];
        }
        catch(ModelNotFoundException $e)  {
            return [ 'message' => $e->getMessage() ];
        }
        
    }

    /** DELETE USER BY ID*/
    public function destroy($id)
    {
        $this->authorize('isAdmin');

        try {
            $user = User::findOrFail($id);

            try {
                $user->delete();
                return [ 'message' => 'User success deleted'];
            }
            catch(QueryException $qe) {
                return [ 'message' => $qe->getMessage() ];
            }
        } 
        catch (ModelNotFoundException $e) {
            return [ 'message' => $e->getMessage() ];
        }
    }
}
